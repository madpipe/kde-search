#ifndef SEARCHRESOURCE_H
#define SEARCHRESOURCE_H

#include <QtQuick/QQuickItem>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>

class SearchResource : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QSortFilterProxyModel* myModel READ myModel WRITE setMyModel NOTIFY myModelChanged)

public:
    explicit SearchResource(QQuickItem *parent = 0);
    ~SearchResource();

    QSortFilterProxyModel* myModel();
    void setMyModel(QSortFilterProxyModel* myModel);
signals:
    void myModelChanged(QSortFilterProxyModel* myModel);

public slots:
    void addElement(const QString value);
    void deleteElement(const int index);
    void editElement(const int index, const QString value);
    void filterByString(const QString value);
    void readSettings();
    void writeSettings();
private:
    QStandardItemModel* m_model;
    QSortFilterProxyModel* m_proxyModel;
};

#endif // SEARCHRESOURCE_H
