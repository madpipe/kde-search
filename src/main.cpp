/*
 * Test app for KDE Search
 * First test: Search a list of names
 */
#include "searchresource.h"

#include <QQmlContext>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

class KSearch
{
public:
    /// The tag that constitutes a search querry component
    class SearchTag
    {
    public:
        QString tagName;
        QString value;
    };

    /// Holds one component of the search querry
    class SearchComponent
    {
    public:
        /// Contains the tags that are related (Who <-> From <-> To)
        QList<SearchTag> tagGroup;
        SearchTag* defaultTag;
    };

    /// Holds the filters for a search querry
    QList<SearchComponent> searchQuerry;
};

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

//     qmlRegisterType<SearchResource>("MySearch", 1, 0, "SearchResource");
    qmlRegisterType<QSortFilterProxyModel>();

    QQmlApplicationEngine engine;
    SearchResource sr;
    engine.rootContext()->setContextProperty("searchR", &sr);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
