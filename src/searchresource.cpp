#include "searchresource.h"

#include <QSettings>

SearchResource::SearchResource(QQuickItem *parent)
    : QQuickItem(parent)
{
    m_model = new QStandardItemModel(this);
    m_proxyModel = new QSortFilterProxyModel(this);
    m_proxyModel->setSourceModel(m_model);

    readSettings();
}

SearchResource::~SearchResource()
{
    writeSettings();
}

QSortFilterProxyModel *SearchResource::myModel()
{
    return this->m_proxyModel;
}

void SearchResource::setMyModel(QSortFilterProxyModel *myModel)
{
    // TODO do some checking here
    this->m_proxyModel = myModel;
    writeSettings();
}

void SearchResource::addElement(const QString value)
{
    m_model->appendRow(new QStandardItem(value));
    writeSettings();
}

void SearchResource::deleteElement(const int index)
{
    m_model->removeRow(index);
    writeSettings();
}

void SearchResource::editElement(const int index, const QString value)
{
    m_model->item(index)->setText(value);
    writeSettings();
}

void SearchResource::filterByString(const QString value)
{
    m_proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    m_proxyModel->setFilterFixedString(value);
}

void SearchResource::readSettings()
{
//     m_model->appendRow(new QStandardItem("Mary"));
//     m_model->appendRow(new QStandardItem("John"));
//     m_model->appendRow(new QStandardItem("Mark"));
//     m_model->appendRow(new QStandardItem("Jude"));
    QSettings settings("KDE", "KSearch");
    settings.beginGroup("DataModel");
    for(int index = 0; index < settings.value("NrOfElements").toInt(); index++)
    {
        QString str = settings.value(QString::number(index)).toString();
        m_model->appendRow(new QStandardItem(str));
    }
    settings.endGroup();
}

void SearchResource::writeSettings()
{
    QSettings settings("KDE", "KSearch");
    settings.beginGroup("DataModel");
    for (int index = 0; index < m_model->rowCount(); index++)
    {
        settings.setValue(QString::number(index),
                          m_model->item(index)->text());
    }
    settings.setValue("NrOfElements", m_model->rowCount());
    settings.endGroup();
}
