/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2015  Ovidiu-Florin BOGDAN <ovidiu.b13@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
// import MySearch 1.0


Window {
    title: qsTr("KDE Search (experiment)")
    visible: true
    width: 360
    height: 200

    property variant tagNames: ["Who", "When", "Where"]

    Rectangle {
        property alias submitBtn: submitBtn
        property alias inputField: inputField
        property alias elements: elements
        color: "#1aa1f7"
        anchors.fill: parent

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 5

            RowLayout {
                id: rowLayout1
                TextField {
                    id: inputField
                    placeholderText: qsTr("Search or add")
                    onTextChanged: {
                        currentTag.text = ""
                        if (inputField.text.length > 0)
                        {
                            for (var i=0; i < tagNames.length; i++)
                            {
                                var str = tagNames[i].substr(0, inputField.text.length)
                                if (str.toLowerCase() === inputField.text.toLowerCase())
                                {
                                    if (currentTag.text.length > 0)
                                        currentTag.text += " "
                                    currentTag.text += tagNames[i]
                                }
                            }
                        }
                        // on edit filter results
//                         searchR.filterByString(text)
                    }
                }

                // on click add new element
                Button {
                    id: submitBtn
                    text: qsTr("Add element")
                    onClicked: {
                        if (inputField.text.length > 0) {
                            searchR.addElement(inputField.text);
                            inputField.text = "";
                        }
                    }
                }
            }


            Rectangle {
                Layout.fillWidth: true
                border.width: 1
                border.color: "grey"
                color: "yellow"
                height: 40
                Flow {
                    anchors.fill: parent
                    spacing: 5
                    Repeater {
                        id: suggestionTagsList
                        model: ListModel {
                            ListElement { name: "Who" }
                            ListElement { name: "When" }
                            ListElement { name: "Where" }
                            ListElement { name: "Who" }
                            ListElement { name: "When" }
                            ListElement { name: "Where" }
                            ListElement { name: "Who" }
                            ListElement { name: "When" }
                            ListElement { name: "Where" }
                            ListElement { name: "Who" }
                            ListElement { name: "When" }
                            ListElement { name: "Where" }
                            ListElement { name: "Who" }
                            ListElement { name: "When" }
                            ListElement { name: "Where" }
                        }
                        delegate: Text {
                                id: suggestionTagText
                                text: name
                        }
                    }
                }
            }
//             Rectangle {
//                 visible: currentTag.text.length > 0 ? true : false
//                 height: currentTag.height
//                 width: currentTag.width
//                 border.width: 1
//                 border.color: "grey"
//                 color: "green"
//                 Text {
//                     id: currentTag
//                     text: ""
//                 }
//             }

            ListView {
                id: elements
                Layout.fillHeight: true
                Layout.fillWidth: true
                clip: true
                model: searchR.myModel
                delegate: Item {
                    height: 30
                    width: parent.width
                    Row {
                        width: parent.width
                        height: parent.height
                        Text {
                            id: txt
                            text: display
                            width: parent.width - (edtBtn.width + delBtn.width)
                            anchors.verticalCenter: parent.verticalCenter
                        }
                        TextField {
                            id: txtEdit
                            text: display
                            width: parent.width - (edtBtn.width + delBtn.width)
                            anchors.verticalCenter: parent.verticalCenter
                            visible: false
                        }
                        Button {
                            id: edtBtn
                            text: "Edit"
                            height: parent.height / 2 + 5
                            anchors.verticalCenter: parent.verticalCenter
                            onClicked: {
                                if (text == "Save")
                                {
                                    searchR.editElement(index, txtEdit.text);
                                    text = "Edit";
                                    txt.visible = true;
                                    txtEdit.visible = false;
                                }
                                else if (text == "Edit")
                                {
                                    text = "Save";
                                    txt.visible = false;
                                    txtEdit.visible = true;
                                }
                            }
                        }
                        Button {
                            id: delBtn
                            text: "Delete"
                            height: parent.height / 2 + 5
                            anchors.verticalCenter: parent.verticalCenter
                            onClicked: {
                                searchR.deleteElement(index);
                            }
                        }
                    }
                }
            }
        }
    }
}
