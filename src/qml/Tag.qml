/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2015  Ovidiu-Florin BOGDAN <ovidiu.b13@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3

RowLayout {
    property alias name : tn_text.text
    property alias value : tv_edit.text

    Rectangle {
        border.width: 1
        color: "azure"
        border.color: "grey"
        width: tn_text.width
        height: tn_text.height

        Text {
            anchors.centerIn: parent
            id: tn_text
            text: qsTr("name")
        }
    }

    Rectangle {
        border.width: 1
        color: "azure"
        border.color: "grey"

        width: tv_edit.visible ? tv_edit.width : tv_view.width
        height: tv_edit.height

        Text {
            anchors.centerIn: parent
            id: tv_view
            text: value
            visible: false
        }
        TextField {
            anchors.centerIn: parent
            id: tv_edit
            text: "a"
            Keys.onPressed: {
                if (event.key == Qt.Key_Enter || event.key == Qt.Key_Return)
                {
                    tv_edit.visible = false;
                    tv_view.visible = true;
                }
            }
        }
    }
}
