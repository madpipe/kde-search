kde-search
==========

**Final Purpose:** A framework that provides a GUI to be used by KMail and other KDE applications to build queries that help searching within their content.

**Currently:** A learning ground for QML and C++ interconnectivity and communication.
